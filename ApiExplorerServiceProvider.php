<?php


namespace Czone\ApiExplorer;

use Illuminate\Support\ServiceProvider;

class ApiExplorerServiceProvider extends ServiceProvider {

	public function boot()
	{
		$this->loadRoutesFrom(__DIR__.'/routes.php');
		$this->loadViewsFrom(__DIR__.'/views', 'apiExplorer');
	}
}