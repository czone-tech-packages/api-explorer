<?php

\Route::group(['middleware' => ['web']], function (){

	\Route::get('/api-explorer', ['as' => 'apiExplorer.index', 'uses' => Czone\ApiExplorer\Controllers\ApiExplorerController::class.'@index']);

});
