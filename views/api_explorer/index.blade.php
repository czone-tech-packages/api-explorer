
@php
    $collect = [];
$routes = $routes->getRoutesByMethod();

//dd($searchIn);

$searchIn  = collect(array_merge($routes['GET'], $routes['POST']))->sort();
@endphp

@extends('layouts.app')
@section('title')
Api Explorer
@endsection
@section('content')
    <div class="row">
        <div class="col-md-4">
            <ul>
                @foreach($searchIn as $key => $route)
                    @if(strpos($key, 'api/')!==false)
                        <li><a onclick="return linkClicked('{{url($key)}}')" href="{{url($key)}}">{{$key}} ({{array_key_exists($key, $routes['GET'])?'GET':(array_key_exists($key, $routes['POST'])?'POST':'')}})</a></li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="col-md-8">
            <div>
                Url: <span id="uri">
                    <input class="form-control" type="text">
                </span>
            </div>
            <div>
                <form id="form-data" action="" onsubmit="fireRequest(this, event)">
                    <div class="row">
                        <div class="col">
                            User Authentication Token(For Auth Routes)
                        </div>
                        <div class="col">
                            <input type="text" name="access_token" class="form-control" placeholder="value">
                        </div>
                    </div>

                    <div v-for="index in rows" :key="index" class="row">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="attribute">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" placeholder="value">
                        </div>
                    </div>

                    <hr/>

                    <div class="row">
                        <div class="col">
                            <button class="btn btn-primary" @click="add()" type="button">Add <i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col">
                            <button class="btn btn-danger" @click="remove()" type="button">Remove <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <hr/>

                    <div class="row">
                        <div class="col">
                            <select name="method" class="form-control">
                                <option value="get">GET</option>
                                <option value="post">POST</option>
                            </select>
                        </div>
                        <div class="col">
                            Method
                        </div>
                    </div>
                    <hr/>
                   <div class="row">
                       <div class="col">
                           <button class="btn btn-primary" type="submit">Submit</button>
                           <button class="btn btn-default" type="reset">Reset</button>
                       </div>
                   </div>
                </form>
            </div>
            <div style="min-height: 100px;">
                <h3>Response</h3>
                <pre id="response" style="overflow: scroll; max-height: 300px; max-width: 100%"></pre>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        function linkClicked(link)
        {
            $('#uri>input').val(link);
            $("#form-data").trigger("reset")
            return false;
        }

        function fireRequest(form, e) {
            e.preventDefault();
            formData = {};
            $("#form-data .row").each(function(index, ele){
                if($(ele).find('input:eq(0)').val())
                {
                    formData[$(ele).find('input:eq(0)').val()] = $(ele).find('input:eq(1)').val();
                }
            })

            var method = $('#form-data select[name=method]').val()
            $('#response').html("");
            var url = $('#uri>input').val()
            var accessToken = $('#form-data input[name="access_token"]').val()
            if(url){
                $.ajax(url, {
                    method: method,
                    data: formData,

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader ("Authorization", "Bearer "+accessToken);
                    }
                })
                        .done(function (response) {
                            $('#response').html(JSON.stringify(response, undefined, 4));
                            toastr.success('Success', 'Success')
                        })
                        .fail(function (error) {
                            $('#response').html(JSON.stringify(error, undefined, 4));
                            toastr.error('Error Occured, please check', 'Error');
                        })
            }else{
                //$('#response').html("<span class='text-danger'>Please select url</span>");
                toastr.error('Please select url', 'Error');
            }

        }

        var vm = new Vue({
            el: "#form-data",
            data:{
                rows: 2
            },
            methods:{
                add: function(){
                    this.rows++
                },
                remove: function(){
                    if(this.rows>=1)
                    {
                        this.rows--
                    }
                }
            }
        });

        //# sourceURL=api_explorer.js
    </script>
@endpush
