<?php


namespace Czone\ApiExplorer\Controllers;

use App\Http\Controllers\Controller;

class ApiExplorerController extends Controller {

	public function index() {

		$routes = \Route::getRoutes();

		return view( 'apiExplorer::api_explorer.index', compact( 'routes' ) );
	}
}